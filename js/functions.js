function init() {
	$.getJSON( "Library/Books.json", function( data ) {
	var items = [];
	var categories = [];
	/*
	for (var i in data['books']) {
		alert(i);
	}
	*/
	/*
	Populate the products/categories div
	*/
	$.each( data.books, function( i, book ) {
		//alert(book['title']);
		var cat = book['category'];
		cat = cat.replace(/ /g, '_');
		items.push(generateProductDiv(book));
		var current = "<li name='" + cat + "'>" + book['category'] + "</li>";
		if($.inArray(current, categories) === -1)
		{
			categories.push(current);
		}
	});
	items.push("<div class='clear'></div>");
	$(".products")[0].innerHTML = "";
	$(".categories")[0].innerHTML = "";
	for(var i=0; i<categories.length; i++)
	{
		$(".categories").get(0).innerHTML += categories[i];
	}
	for(var i=0; i<items.length; i++)
	{
		$(".products").get(0).innerHTML += items[i];
	}
	});
	$(document).on("click", ".infobox button", function(e)
	{
		addToCart($(this).attr("name"));
		e.stopPropagation();
	});
}

function generateProductDiv(book)
{
	var cat = book['category'];
	cat = cat.replace(/ /g, '_');
	return "<div class='lightbox infobox " + cat + "'><img src='images/Books/" + book['id'] + ".jpg'></img><h5>" + book['title'] + "</h5><center><h4>$" + book['price'] + "</h4></center><button type='button' class='btn btn-default' aria-haspopup='true' name='" + book['id'] + "' aria-expanded='false'>Add to cart <span class='glyphicon glyphicon-shopping-cart'></span></button><div class='lightbox-more'><div class='col-sm-14 text-center'><img class='img-responsive text-center center-block' src='images/Books/" + book['id'] + ".jpg' /></div><br />			<div class='text-center'> <h2>" + book['title'] + "</h2><p>Description: " + book['desc'] + "</p><p>Category: " + book['category'] + "</p><h4>Price: $" + book['price'] + "</h4></div><button style='position:absolute;bottom:10px;right:10px;' class='btn btn-primary' type='button' onClick='addToCart(" + book['id'] + ")'>Add to cart</button></div></div>";
}

function getBook(id)
{
	$.getJSON( "Library/Books.json", function( data ) {
		$.each( data.books, function( i, book ) 
		{
			if(id == book['id'])
			{
				return {'id' : book['id']};
			}
		});
	});
	return null;
}

function goTop(elemt)
{
	$('html, body').animate({
		scrollTop: "0px"
	}, 800);
}

function initDeal()
{

$(".deal")[0].innerHTML = "";
$.getJSON( "Library/Books.json", function( data ) {
	$.each( data.deal, function(i, book)
	{
		//$(".deal")[0].innerHTML += "<div class='lightbox'><img src='images/Books/" + book['id'] + ".jpg'></img></div>";
		var bookObj = data.books[book['id']];
		$(".deal")[0].innerHTML += generateProductDiv(bookObj);
	});
});
}

function initPopular()
{
	$(".popular")[0].innerHTML = "";
	$.getJSON( "Library/Books.json", function( data ) {
		$.each( data.popular, function(i, book)
		{
			var bookObj = data.books[book['id']];
			//$(".popular")[0].innerHTML += "<div class='lightbox'><img src='images/Books/" + book['id'] + ".jpg'></img></div>";
			$(".popular")[0].innerHTML += generateProductDiv(bookObj);
		});	
	});
}

$( document ).ready(function() {
	
	$("body")[0].innerHTML += "<div class='lightbox-lightbox'></div>";
	
	$(document).on('click', ".lightbox", function(event) {
		var lightboxMore = $(this).find(".lightbox-more");
		if(lightboxMore.length > 0)
		{
			lightboxMore.css({"display" : "block"});
		} else
		{
			var newEl = $(this).find("img").clone(true).addClass("lightbox-imageonly").appendTo($(this));
		}
		$("body").find(".lightbox-lightbox").css({"display" : "block"});
		event.stopPropagation();
	});
	
	$(document).on('click', '.lightbox-lightbox', function()
	{
		$(document).find(".lightbox-more").css({"display" : "none"});
		$(this).css({"display" : "none"});
	});
	//Toggle which products are shown
	$(document).on('click', '.categories li', function(event)
	{
		var name = $(this).attr("name");
		if($(this).css("color").replace(/\D+/g, '') === '128128128')
		{
			$(this).css({"color" : "black"});
			$(".products").find("." + name).css({"display" : "block"});
		} else
		{
			$(".products").find("." + name).css({"display" : "none"});
			$(this).css({"color" : "gray"});
		}
		event.stopPropagation();
	});	
	/*
	//Show the categories dropdown
	$(".categories-wrapper").hover(function(event)
	{
		var categories = $(this).find(".categories");
		if(categories.css("display") != "block")
		{
			categories.css({"display" : "block"});
		}
		
	});

	
	//Hide the categories dropdown when clicked outside of categories container

	$(document).click(function(event)
	{
		var container = $(".categories");
		container.css({"display" : "none"});
	});
	*/
});