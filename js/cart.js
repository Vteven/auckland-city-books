$(document).ready(function()
{/*
	$(".cart-button").click(function(event)
	{
		var cart = $(".cart");
		var html = "<table>";
		if(cart.css("display") == "block")
		{
			cart.css({"display" : "none"});
		}
		else
		{
			cart.css({"display" : "block"});
			//SOME CODE TO UPDATE THE CART
		}
		html += "</table>"
		//event.stopPropagation();
	});

*/
	$(document).on("click", ".cart-icon", function(e)
	{
		$(this).parent().parent().remove();
		removeFromCart($(this).attr("name"));
		e.stopPropagation();
	});
	
	/*
	$("html").click(function(e)
	{
		if(!($(e).target.hasClass(".cart-icon")))
		{
			$(".cart").css({"display" : "none"});
		}
	});
	
	
*/
	if(!isCartEmpty())
	{
		getCart();
	}
	
	//addToCart(1);
});

var total = 0;
function getCart()
{
	/*
	if(jQuery.isEmptyObject($.cookie("cart")))
	{
		return [];
	}
	*/
	var cart = getCartElements();
	//$(".cart table tr").remove();
	$(".cart table tr").remove();
	$.getJSON( "Library/Books.json", function( data ) {
		$(data.books).each(function(o, book)
		{
			for(var i=0;i<cart.length;i++)
			{
				if(cart[i] != null &&  book['id'] == cart[i])
				{
					$(".cart table").append("<tr><td><img src='images/Books/" + book['id'] + ".jpg' height='32px' width='32px'></img></td><td><h5>" + book['title'] + "</h5></td><td><h4> $" + book['price'] + "</h4></td><td><i class='fa fa-check cart-icon' aria-hidden='true' name='"+ book['id'] +"'></i></td></tr>");
					total = total + parseFloat(book['price']);
				}
			}
		});
		//$(".cart table").append("<tr><td><img src='images/Books/" + book['id'] + ".jpg' height='32px' width='32px'></img></td><td><h5>" + book['title'] + "</h5></td><td><h4>" + book['price'] + "</h4></td><td><i class='fa fa-check cart-icon' aria-hidden='true' name='"+ book['id'] +"'></i></td></tr>");

		//$(".cart table").append("<tr><td><img src='images/Books/" + b['id'] + ".jpg' height='32px' width='32px'></img></td><td><h5>" + b['title'] + "</h5></td><td><h4>" + b['price'] + "</h4></td><td><i class='fa fa-check cart-icon' aria-hidden='true' name='"+ b['id'] +"'></i></td></tr>");

	});
	$(".cart table")[0].innerHTML += "<tr><td></td><td>Subtotal $" + total + "</td></tr>";
	refreshCartButton();
	return cart;
}

function isCartEmpty()
{
	if(readCookie("cart") != null && readCookie("cart") != "")
	{
		return false;
	}
	return true;
}

function refreshCartButton()
{
	$(".cart-button .badge").each(function(i, el)
	{
		if(!isCartEmpty())
		{
			el.innerHTML = getCartElements().length;
		} else
		{
			el.innerHTML = 0;
			$(".cart table")[i].innerHTML = "<tr><td>It's a bit empty</td></tr>";
		}
	});
}

function addToCart(book)
{
	if(book == null || book < 1)
	{
		return;
	}
	if(readCookie("cart") == null)
	{
		var cart = [];
		cart.push(book);
		createCookie("cart", cart, 1);
	} else
	{
		var cart = getCartElements();
		cart.push(book);
		createCookie("cart", cart, 1);
	}
	getCart();
}

function getCartElements()
{
	var cart = readCookie("cart");
	cart = trimChar(cart, ",");
	cart = cart.split(",");
	if(cart.length < 1 || (cart.length > 1 && cart[0] == "")) return [];
	return cart;
}

function removeFromCart(book)
{
	var cart = readCookie("cart").split(",");
	var newCart = [];
	for(var i=0;i<cart.length;i++)
	{
		var bookNo = cart[i];
		if(bookNo != null)
		{
			if(bookNo != book)
			{
				newCart.push(bookNo);
			}
		}
	}
	createCookie("cart", newCart, 1);
	refreshCartButton();
}

function trimChar(string, charToRemove) {
    if (charToRemove === "") return string;
    var start = 0, end = string.length-1;
    while (string.charAt(end) == charToRemove) end--;
    if (end === -1) return "";
    while (string.charAt(start) == charToRemove) start++;
    return string.slice(start, end+1);
}

function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}
