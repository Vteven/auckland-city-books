$(document).ready(function(){
	var firstName = document.forms["subscribeForm"]["FirstName"];
	var lastName = document.forms["subscribeForm"]["LastName"];
	var email = document.forms["subscribeForm"]["Email"];
	var dateOfBirth = document.forms["subscribeForm"]["Date"];
	email.value = "Email:";
	firstName.value = "First Name:";
	lastName.value = "Last Name:";
	dateOfBirth.value = "Date of Birth:"
	email.style.color = "#646464";
	firstName.style.color = "#646464";
	lastName.style.color = "#646464";
	dateOfBirth.style.color = "#646464";
});

function validateForm()
{
	var firstName = document.forms["subscribeForm"]["FirstName"].value;
	var lastName = document.forms["subscribeForm"]["LastName"].value;
	var email = document.forms["subscribeForm"]["Email"].value;
	var DOBValue = document.forms["subscribeForm"]["Date"].value;
	var NameError = document.getElementById("NameError");
	var emailError = document.getElementById("EmailError");
	var termsError = document.getElementById("TermsError");
	var DOBError = document.getElementById("DateError");
	var termsNO = document.getElementById("no");
	var termsYES = document.getElementById("yes");
	checkName(firstName,lastName, NameError);
	var emailEXP = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	var numericalEXP = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
	if (emailEXP.test(email) == false)
	{
		emailError.innerHTML = "The email address entered was incorrectly formatted or invalid";
	}
	else
	{
		emailError.innerHTML = "";
	}
	if(termsNO.checked)
	{
		termsError.innerHTML = "You have to agree to the terms and conditions.";
	}
	else
	{
		termsError.innerHTML = "";
	}
	if(DOBValue.length != 10 || numericalEXP.test(DOBValue) == false)
	{
		DOBError.innerHTML = "The date of birth must be entered in the format: dd/mm/yyyy";
	}
	else
	{
		DOBError.innerHTML = "";
	}
	return false;
}

function checkName(fName,lName, nameError)
{
	if(fName.length > 15 || lName.length > 15)
	{
		nameError.innerHTML = "The name entered cannot exceed 15 characters in length."
	}
	else if(fName == "First Name:"  || lName == "Last Name:")
	{
		nameError.innerHTML = "The first name and last name fields cannot be left blank."
	}
	
	else if(fName.length < 3 || lName.length < 3)
	{
		nameError.innerHTML = "The name entered must be at least 4 characters long."
	}
	else
	{
		nameError.innerHTML = "";
	}
}

function editContent(ID)
{
	ID.style.color = "black";
	ID.value = "";
}

function haultEdit(ID)
{
	var firstName = document.forms["subscribeForm"]["FirstName"];
	var lastName = document.forms["subscribeForm"]["LastName"];
	var email = document.forms["subscribeForm"]["Email"];
	var dateOfBirth = document.forms["subscribeForm"]["Date"];
	ID.style.color = "#646464";
	if(ID.value == "")
	{
		if(ID == firstName)
		{
			ID.value = "First Name:";
		}
		else if(ID == lastName)
		{
			ID.value = "Last Name:";
		}
		else if(ID == email)
		{
			ID.value = "Email:";
		}
		else if(ID == dateOfBirth)
		{
			ID.value = "Date of Birth:";
		}
	}
	//ID.value = "butt";
}
function redICON()
{
	var icon = document.getElementById("closeIconImage");
	icon.src = "../Images/Close-icon-RED.png";
}

function blackICON()
{
	var icon = document.getElementById("closeIconImage");
	icon.src = "../Images/Close-icon.png";
}

function redPolicyICON()
{
	var icon = document.getElementById("closePolicyIconImage");
	icon.src = "../Images/Close-icon-RED.png";
}

function blackPolicyICON()
{
	var icon = document.getElementById("closePolicyIconImage");
	icon.src = "../Images/Close-icon.png";
}

function closeFrames()
{
	var termsAndConditions = document.getElementById("termsAndConditions");
	var privacyPolicy = document.getElementById("privacyPolicy");
	var castShadow = document.getElementById("castShadow");
	termsAndConditions.style.display = "none";
	privacyPolicy.style.display = "none";
	castShadow.style.display = "none";
}

function showTerms()
{
	var termsAndConditions = document.getElementById("termsAndConditions");
	var privacyPolicy = document.getElementById("privacyPolicy");
	var castShadow = document.getElementById("castShadow");
	termsAndConditions.style.display = "block";
	privacyPolicy.style.display = "none";
	castShadow.style.display = "block";
}
function showPolicy()
{
	var termsAndConditions = document.getElementById("termsAndConditions");
	var privacyPolicy = document.getElementById("privacyPolicy");
	var castShadow = document.getElementById("castShadow");
	var policyFrame = document.getElementById("policyFrame");
	termsAndConditions.style.display = "none";
	privacyPolicy.style.display = "block";
	castShadow.style.display = "block";
	policyFrame.src = "PrivacyPolicy.html";
}
